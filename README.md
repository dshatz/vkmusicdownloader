# README #

An application for downloading MP3 music from VK database after logging in to it. Had also been used in order to generate VK API access tokens for [this](https://bitbucket.org/dshatz/musicdownloader) application to consume.

Generated some revenue, reached over 150-200 thousand downloads on Google Play and served as a convenient tool for 340,000 unique VK users.

After this application was banned from Google Play for copyright infringement, the development was abandoned.