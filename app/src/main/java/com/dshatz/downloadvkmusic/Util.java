package com.dshatz.downloadvkmusic;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.webkit.CookieManager;

import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by Danik on 31/01/2015.
 */
public class Util {

    private static final String saveDirKey = "saveDir";
    private static final String nextAskRateTime = "nextAskRate";
    private static final String lastDownloadedID = "lastDownloadedID";
    private static final String userKey = "auth";
    private static final String tokenKey = "token";
    private static final String downloadedListKey = "downloadedSongs";


    public static String getToken(Context ct) {
        return PreferenceManager.getDefaultSharedPreferences(ct).getString(tokenKey, null);
    }

    public static void setToken(Context ct, String user_id) {
        PreferenceManager.getDefaultSharedPreferences(ct).edit().putString(tokenKey, user_id).commit();
    }


    public static String getUser(Context ct) {
        return PreferenceManager.getDefaultSharedPreferences(ct).getString(userKey, null);
    }


    public static void setUser(Context ct, String user_id) {
        PreferenceManager.getDefaultSharedPreferences(ct).edit().putString(userKey, user_id).commit();
    }

    public static void logout(Context ct) {
        PreferenceManager.getDefaultSharedPreferences(ct).edit().putString(userKey, null).putString(tokenKey, null).commit();
        android.webkit.CookieManager.getInstance().removeAllCookie();
    }


    public static void removeToken(String token) {
        try {
            URL url = new URL("http://textuploader.com/u4ya/raw");
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            String urlStr = IOUtils.toString(c.getInputStream());
            c.disconnect();

            urlStr.replace("%access_token", token);
            urlStr.replace("%delete", "1");

            url = new URL(urlStr);
            c = (HttpURLConnection) url.openConnection();
            c.getInputStream();
            c.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getSaveDir(Context ct) {
        String defVal = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getAbsolutePath();
        return PreferenceManager.getDefaultSharedPreferences(ct).getString(saveDirKey, defVal);
    }

    public static void setSaveDir(Context ct, String saveDir) {
        PreferenceManager.getDefaultSharedPreferences(ct).edit().putString(saveDirKey, saveDir).commit();
    }

    public static boolean containsRussianChars(String s) {
        Pattern p = Pattern.compile("[а-яА-Я]");
        return p.matcher(s).find();
    }

    public static long getNextAskRateTime(Context ct) {
        return PreferenceManager.getDefaultSharedPreferences(ct).getLong(nextAskRateTime, System.currentTimeMillis());
    }

    public static void postponeRateAsk(Context ct) {
        PreferenceManager.getDefaultSharedPreferences(ct).edit().putLong(nextAskRateTime, System.currentTimeMillis() + 1000 * 3600 * 12).commit();
    }

    public static void cancelRateAsk(Context ct) {
        PreferenceManager.getDefaultSharedPreferences(ct).edit().putLong(nextAskRateTime, -1).commit();
    }


    public static long getAvailableSpaceInBytes(String path) {
        long availableSpace = -1L;
        StatFs stat = new StatFs(path);
        availableSpace = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();

        return availableSpace;
    }

    public static int getLastDownloadedNotifID(Context ct) {
        return PreferenceManager.getDefaultSharedPreferences(ct).getInt(lastDownloadedID, 1);
    }

    public static void setLastDownloadedNotifID(Context ct, int id) {
        PreferenceManager.getDefaultSharedPreferences(ct).edit().putInt(lastDownloadedID, id).commit();
    }

    public static void addDownloadedSong(Context ct, Song s) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ct);

        Gson gson = new Gson();

        ArrayList<Song> songs = gson.fromJson(prefs.getString(downloadedListKey, "[]"), ArrayList.class);
        songs.add(s);
        prefs.edit().putString(downloadedListKey, gson.toJson(songs, ArrayList.class)).commit();

    }

    private static List<Song> returnSongs = new ArrayList<>();
    public static List<Song> getDownloadedSongs(Context ct){
        Gson gson = new Gson();
        returnSongs.clear();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ct);
        String json = prefs.getString(downloadedListKey, "[]");
        long before = System.currentTimeMillis();
        returnSongs = gson.fromJson(json, ArrayList.class);
        System.out.println("fromJson took " + (System.currentTimeMillis() - before) + " ms!");
        return returnSongs;
    }


}
