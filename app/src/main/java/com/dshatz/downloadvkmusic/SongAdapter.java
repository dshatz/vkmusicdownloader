package com.dshatz.downloadvkmusic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dshatz.vkmusicdownloader.R;

import java.util.List;

/**
 * Created by Danik on 18/06/2015.
 */
public class SongAdapter extends ArrayAdapter<Song>{


    public SongAdapter(Context context, List<Song> songs) {
        super(context, 0, songs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Song song = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.song_layout, parent, false);
        }

        TextView titleView = (TextView) convertView.findViewById(R.id.titleView);
        TextView artistView = (TextView) convertView.findViewById(R.id.artistView);
        TextView durationView = (TextView) convertView.findViewById(R.id.durationView);

        int duration = song.duration;
        int min = (int)(duration / 60f);
        int s = duration - min * 60;

        titleView.setText(song.title);
        artistView.setText(song.artist);
        durationView.setText(min + ((s < 10) ? ":0" : ":") + s);

        return convertView;
    }
}
