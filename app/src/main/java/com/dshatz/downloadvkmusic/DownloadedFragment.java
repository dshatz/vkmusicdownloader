package com.dshatz.downloadvkmusic;

import android.content.ContentProvider;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.dshatz.vkmusicdownloader.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danik on 07/07/2015.
 */
public class DownloadedFragment extends Fragment implements AdapterView.OnItemClickListener {


    private ListView listView;
    private EditText filterField;

    private SongAdapter adapter;
    private List<Song> songs;
    private List<Song> filteredSongs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_downloaded, container, false);
        listView = (ListView) v.findViewById(R.id.searchResults);
        filterField = (EditText) v.findViewById(R.id.filterField);

        songs = new ArrayList<>();
        filteredSongs = new ArrayList<>();
        adapter = new SongAdapter(getActivity(), filteredSongs);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);

        showData();
        return v;
    }


    public void showData(){
        songs = Util.getDownloadedSongs(getActivity());
        adapter.notifyDataSetChanged();
    }

    public void filter(){
        String filter = filterField.getText().toString();
        filteredSongs.clear();

        if(filter.isEmpty()){
            for(int i = 0; i < songs.size(); i++){
                filteredSongs.add(songs.get(i));
            }
            adapter.notifyDataSetChanged();
            return;
        }

        String[] filter_parts = filter.split(" ");
        for(int i = 0; i < filter_parts.length; i++){
            filter_parts[i] = filter_parts[i].toLowerCase();
        }

        for(Song song : songs){
            String data = song.title + " " + song.artist;
            data = data.toLowerCase();
            boolean match = true;
            for(int i=0; i<filter_parts.length; i++){
                if(!data.contains(filter_parts[i])){
                    match = false;
                    break;
                }
            }
            if(match){
                filteredSongs.add(song);
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}