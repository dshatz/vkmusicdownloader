package com.dshatz.downloadvkmusic;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dshatz.vkmusicdownloader.R;
import com.google.android.gms.analytics.HitBuilders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danik on 07/07/2015.
 */
public class PersonalMusicFragment extends Fragment implements AdapterView.OnItemClickListener, Refreshable, TitledFragment{

    private List<Song> songs;
    private List<Song> filtered_songs;
    private SongAdapter adapter;

    private View progressBar;
    private ListView songList;
    private EditText filterText;
    private TextView noResultsView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_personal_music, container, false);

        songs = new ArrayList<Song>();
        filtered_songs = new ArrayList<Song>();


        filterText = (EditText) v.findViewById(R.id.filterField);
        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        songList = (ListView) v.findViewById(R.id.searchResults);
        progressBar = v.findViewById(R.id.progressBar);
        noResultsView = (TextView) v.findViewById(R.id.nopersonalmusic);

        adapter = new SongAdapter(getActivity(), filtered_songs);
        songList.setAdapter(adapter);
        songList.setOnItemClickListener(this);

        listResults();

        return v;
    }

    private void filter(String filter){
        filtered_songs.clear();

        if(filter.isEmpty()){
            for(int i = 0; i < songs.size(); i++){
                filtered_songs.add(songs.get(i));
            }
            adapter.notifyDataSetChanged();
            return;
        }

        String[] filter_parts = filter.split(" ");
        for(int i = 0; i < filter_parts.length; i++){
            filter_parts[i] = filter_parts[i].toLowerCase();
        }

        for(Song song : songs){
            String data = song.title + " " + song.artist;
            data = data.toLowerCase();
            boolean match = true;
            for(int i=0; i<filter_parts.length; i++){
                if(!data.contains(filter_parts[i])){
                    match = false;
                    break;
                }
            }
            if(match){
                filtered_songs.add(song);
            }
        }
        adapter.notifyDataSetChanged();
    }

    private Runnable networkProblemOccurred = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getActivity(), R.string.networkProblem, Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            noResultsView.setVisibility(View.GONE);
        }
    };

    private Runnable parsingErrorOccured = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getActivity(), R.string.parsingError, Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            noResultsView.setVisibility(View.GONE);
        }
    };




    public void search() {

        long start = System.currentTimeMillis();

        String urlStr = null;

        try{
            URL addressUrl = new URL("http://textuploader.com/f0xw/raw");
            HttpURLConnection c = (HttpURLConnection) addressUrl.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
            urlStr = reader.readLine();
            reader.close();
            c.disconnect();
            System.out.println("Searching via: " + urlStr);
        } catch (Exception e) {
            e.printStackTrace();
            getActivity().runOnUiThread(networkProblemOccurred);
            return;
        }


        URL url = null;
        try{
            String user_id = Util.getUser(getActivity().getApplicationContext());
            String token = Util.getToken(getActivity().getApplicationContext());
            urlStr = urlStr.replace("%user_id", user_id);
            urlStr = urlStr.replace("%access_token", token);

            url = new URL(urlStr);
            System.out.println("Searching on :" + urlStr);
        }
        catch(Exception e){
            e.printStackTrace();
        }


        String r = null;
        StringBuffer response = new StringBuffer();
        try{
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            con.disconnect();

            r = response.toString();
            System.out.println(r);
            JSONObject obj = new JSONObject(r);
            if(obj.has("error")){

                Util.removeToken(Util.getToken(getActivity().getApplicationContext()));


                Util.logout(getActivity().getApplicationContext());
                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();

            }


        }
        catch(Exception e){
            e.printStackTrace();
            getActivity().runOnUiThread(networkProblemOccurred);
            return;
        }
        try{
            parseResponse(r);
        } catch(Exception e){
            e.printStackTrace();
            getActivity().runOnUiThread(parsingErrorOccured);
            return;
        }


        getActivity().runOnUiThread(showResultsRunnable);

    }



    private Runnable showResultsRunnable = new Runnable() {
        @Override
        public void run() {
            try{
                System.out.println("Showing results!");
                showResults();
            } catch(JSONException e){
                e.printStackTrace();
            }
        }
    };
    private void showResults() throws JSONException {

        if(songs.size() != 0){
            progressBar.setVisibility(View.GONE);
            songList.setVisibility(View.VISIBLE);
            noResultsView.setVisibility(View.GONE);
            adapter.notifyDataSetChanged();
            filter("");
        }
        else {
            progressBar.setVisibility(View.GONE);
            songList.setVisibility(View.GONE);
            noResultsView.setVisibility(View.VISIBLE);
        }

    }
    public void listResults(){

        progressBar.setVisibility(View.VISIBLE);
        songList.setVisibility(View.GONE);
        noResultsView.setVisibility(View.GONE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                search();
            }
        }).start();
    }

    private int parseResponse(String response){
        songs.clear();
        try{
            JSONObject obj = new JSONObject(response);
            JSONArray array = obj.getJSONArray("response");
            for(int i = 1; i < array.length(); i++){
                JSONObject o = (JSONObject)array.get(i);
                Song song = new Song(o.getString("title"), o.getString("artist"), o.getString("url").replace("\\/", "/"), o.getInt("duration"));
                songs.add(song);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return songs.size();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Song song = filtered_songs.get(position);

        final String title = song.title;
        final String url = song.url;
        final String artist = song.artist;



        SongInfoDialog dialog = ((MainActivity) getActivity()).getDialog();
        dialog.reset(song);
        dialog.show();
    }




    @Override
    public void refresh() {
        listResults();
    }


}
