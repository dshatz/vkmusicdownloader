package com.dshatz.downloadvkmusic;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dshatz.vkmusicdownloader.R;

import java.util.HashMap;

/**
 * Created by Danik on 07/07/2015.
 */
public class TabPagerAdapter extends FragmentPagerAdapter {

    public HashMap<Class<?>, Fragment> fragments;

    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
        fragments = new HashMap<>();
    }

    @Override
    public Fragment getItem(int i) {
        Class<?> c;
        switch(i){
            case 0:
                c = PersonalMusicFragment.class;
                break;
            case 1:
                c = SearchFragment.class;
                break;
            case 2:
                c = DownloadedFragment.class;
                break;
            default:
                c = PersonalMusicFragment.class;
                break;
        }
        return getFragmentByClass(c);

    }

    private Fragment getFragmentByClass(Class<?> c) {
        if(fragments.containsKey(c)) return fragments.get(c);
        else {
            Fragment f = null;
            try {
                f = (Fragment) c.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            fragments.put(c, f);
            return f;
        }
    }

    public int getTitle(int i){
        switch (i){
            case 0:
                return R.string.title_activity_personal_music;
            case 1:
                return R.string.goToGlobalSearch;
            case 2:
                return R.string.fragment_downloaded_title;
        }
        return 0;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
