package com.dshatz.downloadvkmusic;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dshatz.vkmusicdownloader.R;


public class LoginActivity extends ActionBarActivity {


    public LoginActivity() {
    }


    private static final String serverTokenUrl = "http://oauth.vk.com/authorize/?client_id=4898372&scope=audio,offline&redirect_uri=http://music.dshatz.com/onAuth.php&display=mobile&v=5.30&response_type=token";
    private static final String clientTokenUrl = "http://oauth.vk.com/authorize/?client_id=4898372&scope=audio,offline&redirect_uri=http://music.dshatz.com/onAuth.php?return=1&display=mobile&v=5.30&response_type=token";


    private WebView wv;
    private ProgressDialog dialog;

    private enum LoginStep{
        TOKEN_FOR_SERVER,
        TOKEN_FOR_USER
    }

    private LoginStep loginStep;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginStep = LoginStep.TOKEN_FOR_SERVER;

        if(Util.getUser(getApplicationContext()) != null){
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
            return;
        }
        setContentView(R.layout.activity_login);
        wv = (WebView) findViewById(R.id.webview);

        dialog = ProgressDialog.show(this, getResources().getString(R.string.loading), "", true);

        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setSavePassword(false);
        wv.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                dialog.show();
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                dialog.dismiss();
                if (url.startsWith("http://music.dshatz.com/tokenRecorder_v2.php?")) {

                    System.out.println("Server authorization: " + url);

                    wv.loadUrl(clientTokenUrl);
                }
                else if(url.startsWith("http://music.dshatz.com/tokenReceiver.php?")){
                    System.out.println("Client authorization: " + url);
                    String paramsStr = url.substring("http://music.dshatz.com/tokenReceiver.php?".length(), url.length());
                    String[] params = paramsStr.split("\\&");

                    String token = null;
                    String user = null;
                    for (int i = 0; i < params.length; i++) {
                        String[] parts = params[i].split("=");
                        if (parts[0].equals("user_id")) user = parts[1];
                        if(parts[0].equals("access_token")) token = parts[1];
                    }
                    Util.setUser(getApplicationContext(), user);
                    Util.setToken(getApplicationContext(), token);

                    System.out.println("Authorization complete! user_id=" + user + " and token = " + token);

                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
                super.onPageFinished(view, url);
            }
        });



        wv.loadUrl(serverTokenUrl);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }
}
