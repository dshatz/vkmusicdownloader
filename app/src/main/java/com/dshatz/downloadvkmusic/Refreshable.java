package com.dshatz.downloadvkmusic;

/**
 * Created by Danik on 07/07/2015.
 */
public interface Refreshable {

    void refresh();

}
