package com.dshatz.downloadvkmusic;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.dshatz.vkmusicdownloader.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import net.rdrei.android.dirchooser.DirectoryChooserFragment;

import java.sql.Ref;

import javax.annotation.Nonnull;

/**
 * Created by Danik on 27/06/2015.
 */
public class MainActivity extends AppCompatActivity implements DirectoryChooserFragment.OnFragmentInteractionListener, TabLayout.OnTabSelectedListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager pager;
    private TabPagerAdapter adapter;

    private Tracker tracker;
    private AdView adView;

    private DirectoryChooserFragment dirChooserFragment;

    private SongInfoDialog dialog;

    public static MainActivity instance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        instance = this;


        if(Util.getUser(getApplicationContext()) == null){
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
            finish();
            return;
        }

        dialog = new SongInfoDialog(this);

        tracker = ((App) getApplication()).getTracker(App.TrackerName.APP_TRACKER);
        tracker.enableAdvertisingIdCollection(true);

        dirChooserFragment = DirectoryChooserFragment.newInstance(getResources().getString(R.string.folder_name), Util.getSaveDir(this));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        pager = (ViewPager) findViewById(R.id.pager);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        adapter = new TabPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager);
        adapter.notifyDataSetChanged();

        for(int i = 0; i < tabLayout.getTabCount(); i++){
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setText(adapter.getTitle(i));
        }

        tabLayout.setOnTabSelectedListener(this);

        adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    public android.support.v4.app.Fragment getCurrentFragment(){
        int index = pager.getCurrentItem();
        return adapter.getItem(index);
    }

    public SongInfoDialog getDialog(){
        return dialog;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_changeDir) {
            dirChooserFragment.show(getSupportFragmentManager(), null);
            Toast.makeText(this, R.string.changeDirAffects, Toast.LENGTH_LONG).show();
        }

        if(id == R.id.action_logout) {

            final AlertDialog d = new AlertDialog.Builder(this).setMessage(R.string.reallylogout).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Util.logout(getApplicationContext());
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();

        }

        if(id == R.id.action_refresh){
            Fragment currFragment = getCurrentFragment();
            if(currFragment instanceof Refreshable) ((Refreshable)currFragment).refresh();
        }

        return super.onOptionsItemSelected(item);
    }

    private Menu menu;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_personal_music, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialog.onActivityDestroyed();
    }

    @Override
    public void onSelectDirectory(@Nonnull String path) {
        Util.setSaveDir(this, path);
        dirChooserFragment.dismiss();
    }

    @Override
    public void onCancelChooser() {
        dirChooserFragment.dismiss();
    }


    @Override
    protected void onStart() {
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Fragment f = adapter.getItem(tab.getPosition());
        for(int i = 0; i < menu.size(); i++){
            MenuItem item = menu.getItem(i);
            if(item.getItemId() == R.id.action_refresh){
                item.setVisible(f instanceof Refreshable);
            }
        }
        pager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
