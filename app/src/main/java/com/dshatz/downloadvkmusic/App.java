package com.dshatz.downloadvkmusic;

import android.app.Application;

import com.dshatz.vkmusicdownloader.R;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

/**
 * Created by Danik on 08/02/2015.
 */
public class App extends Application {


    public enum TrackerName {
        APP_TRACKER
    }



    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(R.xml.app_tracker) : null;
            mTrackers.put(trackerId, t);
        }
        return mTrackers.get(trackerId);
    }

}
