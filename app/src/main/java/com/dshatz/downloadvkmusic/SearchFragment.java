package com.dshatz.downloadvkmusic;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dshatz.vkmusicdownloader.R;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public class SearchFragment extends Fragment implements AdapterView.OnItemClickListener {

    private FragmentState state;

    private ListView listView;
    private ImageButton searchButton;
    private TextView noResultFound;
    private ProgressBar progressBar;
    private EditText searchField;

    private List<Song> songs;
    private SongAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        songs = new ArrayList<>();

        View v  = inflater.inflate(R.layout.fragment_search, container, false);
        listView = (ListView) v.findViewById(R.id.searchResults);
        searchButton = (ImageButton) v.findViewById(R.id.searchButton);
        noResultFound = (TextView) v.findViewById(R.id.noresultsview);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        searchField = (EditText) v.findViewById(R.id.searchField);

        adapter = new SongAdapter(getActivity(), songs);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);

        setState(FragmentState.IDLE);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSearch();
            }
        });

        searchField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    startSearch();
                    return true;
                }
                return false;
            }
        });

        return v;
    }

    public void startSearch(){
        setState(FragmentState.LOADING);
        new Thread(new Runnable() {
            @Override
            public void run() {
                search();
            }
        }).start();
    }

    public void setState(FragmentState state){
        this.state = state;
        switch(state){
            case IDLE:
                listView.setVisibility(View.GONE);
                noResultFound.setVisibility(View.GONE);
                searchButton.setEnabled(true);
                searchField.setEnabled(true);
                progressBar.setVisibility(View.GONE);
                break;
            case LOADING:
                listView.setVisibility(View.GONE);
                noResultFound.setVisibility(View.GONE);
                searchButton.setEnabled(false);
                searchField.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
                break;
            case LOADED:
                listView.setVisibility(View.VISIBLE);
                noResultFound.setVisibility(View.GONE);
                searchButton.setEnabled(true);
                searchField.setEnabled(true);
                progressBar.setVisibility(View.GONE);
                break;
            case NORESULTS:
                listView.setVisibility(View.GONE);
                noResultFound.setVisibility(View.VISIBLE);
                searchButton.setEnabled(true);
                searchField.setEnabled(true);
                progressBar.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SongInfoDialog dialog = ((MainActivity) getActivity()).getDialog();
        Song s = songs.get(position);
        dialog.reset(s);
        dialog.show();
    }



    public enum FragmentState {
        IDLE,
        LOADING,
        LOADED,
        NORESULTS
    }

    public void search() {

        long start = System.currentTimeMillis();

        String urlStr = null;

        try{
            URL addressUrl = new URL("http://textuploader.com/i3k2/raw");
            HttpURLConnection c = (HttpURLConnection) addressUrl.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
            urlStr = reader.readLine();
            reader.close();
            c.disconnect();
            System.out.println("Searching via: " + urlStr);
        } catch (Exception e) {
            e.printStackTrace();
            getActivity().runOnUiThread(networkProblemOccurred);
            return;
        }


        URL url = null;
        try{
            String token = Util.getToken(getActivity().getApplicationContext());
            urlStr = urlStr.replace("%query", URLEncoder.encode(searchField.getText().toString()));
            urlStr = urlStr.replace("%access_token", token);

            url = new URL(urlStr);
            System.out.println("Searching on :" + urlStr);
        }
        catch(Exception e){
            e.printStackTrace();
        }


        String r = null;
        StringBuffer response = new StringBuffer();
        try{
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            con.disconnect();

            r = response.toString();
            System.out.println(r);
            JSONObject obj = new JSONObject(r);
            if(obj.has("error")){

                Util.removeToken(Util.getToken(getActivity().getApplicationContext()));


                Util.logout(getActivity().getApplicationContext());
                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();

            }


        }
        catch(Exception e){
            e.printStackTrace();
            getActivity().runOnUiThread(networkProblemOccurred);
            return;
        }
        try{
            if(parseResponse(r) == 0) setState(FragmentState.NORESULTS);
            filterResults();
        } catch(Exception e){
            e.printStackTrace();
            getActivity().runOnUiThread(parsingErrorOccured);
            return;
        }


        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                setState(FragmentState.LOADED);
            }
        });
    }

    private int parseResponse(String response){
        songs.clear();
        try{
            JSONObject obj = new JSONObject(response);
            JSONArray array = obj.getJSONArray("response");
            for(int i = 1; i < array.length(); i++){
                JSONObject o = (JSONObject)array.get(i);
                Song song = new Song(o.getString("title"), o.getString("artist"), o.getString("url").replace("\\/", "/"), o.getInt("duration"));
                songs.add(song);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return songs.size();
    }


    private List<Song> songsToRemove;
    public void filterResults() throws JSONException {

        if(songsToRemove == null) songsToRemove = new ArrayList<>();
        else songsToRemove.clear();

        for(Song s : songs){



            String title = s.title;
            String artist = s.artist;
            if(title.length() > 50) songsToRemove.add(s);
            else if (artist.contains(title) || artist.length() > 20 || title.contains(artist)) songsToRemove.add(s);
            else if((Util.containsRussianChars(title) || Util.containsRussianChars(artist)) ^ Util.containsRussianChars(searchField.getText().toString())) songsToRemove.add(s);
        }

        for(Song s: songsToRemove){
            songs.remove(s);
        }
        songsToRemove.clear();
    }

    private Runnable networkProblemOccurred = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getActivity(), R.string.networkProblem, Toast.LENGTH_LONG).show();
            setState(FragmentState.IDLE);
        }
    };

    private Runnable parsingErrorOccured = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getActivity(), R.string.parsingError, Toast.LENGTH_LONG).show();
            setState(FragmentState.IDLE);
        }
    };


}

