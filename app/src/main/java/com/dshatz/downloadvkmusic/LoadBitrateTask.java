package com.dshatz.downloadvkmusic;

import android.os.AsyncTask;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Danik on 07/07/2015.
 */
public class LoadBitrateTask extends AsyncTask<Song, Void, Integer> {


    @Override
    protected Integer doInBackground(Song... params) {
        Song song = params[0];
        int length = 0;
        try {
            URL url = new URL(song.url);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            length = con.getContentLength();
            con.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        int br = (int) (((float) length * 8 / (float) song.duration) / 1000f);
        song.bitrate = br;
        return br;

    }
}
