package com.dshatz.downloadvkmusic;

/**
 * Created by Danik on 18/06/2015.
 */
public class Song {

    public String title, artist, url;
    public int duration;
    public int bitrate = BITRATE_UNKNOWN;
    public static final int BITRATE_UNKNOWN = -1;
    public String downloadPath;

    public Song(String title, String artist, String url, int duration){
        this.title = title;
        this.artist = artist;
        this.url = url;
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "SONG:"+title+":"+artist+":"+url+":"+duration+":"+bitrate+":"+downloadPath+":SONG";
    }

    public static Song fromString(String s){
        String[] parts = s.split(":");
        if(parts[0].equals("SONG") && parts[7].equals("SONG")){
            Song song = new Song(parts[1], parts[2], parts[3], Integer.parseInt(parts[4]));
            song.bitrate = Integer.parseInt(parts[5]);
            song.downloadPath = parts[6];
            return song;
        }
        else return null;
    }
}
