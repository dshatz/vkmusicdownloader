package com.dshatz.downloadvkmusic;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.dshatz.vkmusicdownloader.R;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.ID3v24Tag;
import com.mpatric.mp3agic.Mp3File;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danik on 07/02/2015.
 */
public class DownloadService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private boolean downloading = false;

    private String currFilePath;
    private Song currSong;
    private List<Song> queue;

    public static final String titleExtra = "title";
    public static final String urlExtra = "url";
    public static final String artistExtra = "artist";

    private NotificationManager notifManager;

    private NotificationCompat.Builder ongoingNotifBuilder;
    private NotificationCompat.Builder downloadingNotifBuider;
    private NotificationCompat.Builder downloadedNotifBuilder;

    private Runnable insufficientMemory = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getBaseContext(), R.string.nospace, Toast.LENGTH_SHORT).show();
        }
    };



    private final int ongoingId = 999;

    private final int downloadingNotifId = 499;

    private int downloadedNotifId = 1;

    private void showDownloadedNotification(String title, String file, boolean failed){
        downloadedNotifBuilder.setContentTitle(title);

        if(failed) downloadedNotifBuilder.setContentText(getResources().getString(R.string.download_failed));
        else downloadedNotifBuilder.setContentText(getResources().getString(R.string.download_complete));

        Intent openMp3Intent = new Intent(android.content.Intent.ACTION_VIEW);
        openMp3Intent.setDataAndType(Uri.fromFile(new File(file)), "audio/mpeg");
        downloadedNotifBuilder.setContentIntent(PendingIntent.getActivity(DownloadService.this, 0, openMp3Intent, 0));

        notifManager.notify(downloadedNotifId, downloadedNotifBuilder.build());
        downloadedNotifId ++;
    }

    private void showDownloadingNotification(int progress, String title){
        downloadingNotifBuider.setContentTitle(title);
        downloadingNotifBuider.setProgress(100, progress, false);
        notifManager.notify(downloadingNotifId, downloadingNotifBuider.build());
    }

    private void cancelDownloadingNotification(){
        notifManager.cancel(downloadingNotifId);
    }

    private void downloadNext(){
        if(queue.size() > 0){
            currSong = queue.get(0);
            queue.remove(0);
            new Thread(download).start();
        }
        else{
            stopSelf();
        }
    }


    public static final String action_cancel = "cancel_download";
    private BroadcastReceiver receiver;

    public void onCreate() {
        super.onCreate();
        System.out.println("onCreate");

        IntentFilter filter = new IntentFilter();
        filter.addAction(action_cancel);
        // Add other actions as needed

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(action_cancel)) {
                    cancelled = true;
                }
            }
        };

        registerReceiver(receiver, filter);


        queue = new ArrayList<>();

        downloadedNotifId = Util.getLastDownloadedNotifID(this);
        notifManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        ongoingNotifBuilder = new NotificationCompat.Builder(this).setOngoing(true).setContentTitle(getResources().getString(R.string.downloading)).setSmallIcon(R.drawable.ic_launcher).setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));
        downloadedNotifBuilder = new NotificationCompat.Builder(this).setOngoing(false).setSmallIcon(android.R.drawable.stat_sys_download_done).setContentText(getResources().getString(R.string.download_complete)).setLargeIcon(BitmapFactory.decodeResource(getResources(), android.R.drawable.stat_sys_download_done));
        downloadingNotifBuider = new NotificationCompat.Builder(this).setOngoing(true).setSmallIcon(android.R.drawable.stat_sys_download).setLargeIcon(BitmapFactory.decodeResource(getResources(), android.R.drawable.stat_sys_download));


        Intent cancelIntent = new Intent(action_cancel);
        PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(this, 0, cancelIntent, 0);

        downloadingNotifBuider.addAction(android.R.drawable.ic_menu_close_clear_cancel, getResources().getString(android.R.string.cancel), cancelPendingIntent);

        ongoingNotifBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0));
        downloadingNotifBuider.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0));

        downloadedNotifBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0));
        downloadedNotifBuilder.setAutoCancel(true);

        startForeground(ongoingId, ongoingNotifBuilder.build());
    }

    private void updateOngoingNotification(){
        ongoingNotifBuilder.setContentText(queue.size() + " " + getResources().getString(R.string.inQueue));
        if(queue.size() == 0) ongoingNotifBuilder.setContentText("");
        startForeground(ongoingId, ongoingNotifBuilder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Song s = MainActivity.instance.getDialog().getSong();

        currSong = s;

        if(!downloading){
            new Thread(download).start();
        }else{
            queue.add(s);
            updateOngoingNotification();
        }
        return super.onStartCommand(intent, flags, startId);
    }


    Runnable showSaveLocationUnaccessible = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(DownloadService.this, R.string.saveLocUnaccessible, Toast.LENGTH_LONG).show();
        }
    };

    private boolean cancelled = false;
    private boolean failed = false;


    Runnable download = new Runnable() {
        @Override
        public void run() {
            downloading = true;

            updateOngoingNotification();

            System.out.println("Starting download task for " + currSong.title);

            showDownloadingNotification(0, currSong.title);

            try {


                currFilePath = Util.getSaveDir(getBaseContext()) + File.separator + currSong.artist + " - " + currSong.title + ".mp3";
                File dest = new File(currFilePath.substring(0, currFilePath.length() - 1));
                File folder = new File(Util.getSaveDir(getBaseContext()));

                try{
                    if(!folder.isDirectory()) folder.mkdirs();
                    if(!dest.exists()) dest.createNewFile();
                } catch(IOException ioe){
                    if(MainActivity.instance != null) MainActivity.instance.runOnUiThread(showSaveLocationUnaccessible);
                    cancelDownloadingNotification();
                    downloading = false;
                    stopForeground(true);
                    return;
                }

                System.out.println("Downloading from: " + currSong.url);

                URL adress = new URL(currSong.url);
                HttpURLConnection con = (HttpURLConnection) adress.openConnection();


                InputStream in = con.getInputStream();
                int contentLength = con.getContentLength();



                if(contentLength > Util.getAvailableSpaceInBytes(dest.getAbsolutePath())) {
                    if(MainActivity.instance != null) MainActivity.instance.runOnUiThread(insufficientMemory);
                }


                showDownloadingNotification(0, currSong.title);


                FileOutputStream out = null;
                if (contentLength != -1) {
                    out = new FileOutputStream(dest);
                }

                byte[] buf = new byte[512];

                int readed = 0;


                int lastPercentage = 0;

                while (true) {
                    if(cancelled) break;

                    int len = 0;
                    try{
                        len = in.read(buf);
                    } catch(Exception e){
                        failed = true;
                        break;
                    }
                    if (len == -1) {
                        break;
                    }
                    out.write(buf, 0, len);
                    readed += len;

                    int percentage = (int)((float)readed/contentLength * 100);

                    if(percentage - lastPercentage > 5){
                        System.out.println(percentage + "% complete: " + currSong.title);
                        showDownloadingNotification(percentage, currSong.title);
                        lastPercentage = percentage;
                    }


                }
                out.close();


                if(!cancelled){
                    System.out.println("Changing MP3 Headers...");

                    Mp3File mp3file = new Mp3File(dest.getAbsolutePath());

                    ID3v2 id3v2Tag;
                    if (mp3file.hasId3v2Tag()) {
                        id3v2Tag = mp3file.getId3v2Tag();
                    } else {
                        // mp3 does not have an ID3v2 tag, let's create one..
                        id3v2Tag = new ID3v24Tag();
                        mp3file.setId3v2Tag(id3v2Tag);
                    }
                    id3v2Tag.setArtist(currSong.artist);
                    id3v2Tag.setTitle(currSong.title);


                    if (mp3file.hasId3v1Tag()) {
                        mp3file.removeId3v1Tag();
                    }
                    if (mp3file.hasCustomTag()) {
                        mp3file.removeCustomTag();
                    }


                    File newFile = new File(currFilePath); //add 3 to *.mp
                    if(!newFile.isFile()){
                        newFile.createNewFile();
                    }

                    currSong.downloadPath = currFilePath;

                    mp3file.save(newFile.getAbsolutePath());

                    System.out.println("Title: " + currSong.title);
                    System.out.println("Artist: " + currSong.artist);

                    System.out.println("MP3 Headers Changed");


                    dest.delete();

                    cancelDownloadingNotification();


                    sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(newFile)));
                }
                else{
                    dest.delete();
                    System.out.println("Download cancelled! Deleting file!");
                }




            } catch (Exception e) {
                e.printStackTrace();
            }


            if(!cancelled) {
                showDownloadedNotification(currSong.title, currFilePath, failed);
                System.out.println("Download finished (" + currSong.title + ")");
            } else {
                cancelDownloadingNotification();
            }

            Util.addDownloadedSong(getApplicationContext(), currSong);
            cancelled = false;
            downloading =false;
            downloadNext();

        }
    };


    @Override
    public void onDestroy() {
        System.out.println("onDestroy");
        stopForeground(true);
        unregisterReceiver(receiver);
        Util.setLastDownloadedNotifID(this, downloadedNotifId);
        super.onDestroy();
    }




}
