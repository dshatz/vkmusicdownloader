package com.dshatz.downloadvkmusic;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dshatz.vkmusicdownloader.R;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Danik on 07/07/2015.
 */
public class SongInfoDialog extends AlertDialog implements SeekBar.OnSeekBarChangeListener, View.OnClickListener, MediaPlayer.OnCompletionListener {

    private SeekBar seekBar;
    private LinearLayout dialogLayout;
    private TextView bitRateView;
    private Button playButton, downloadButton;

    private TextView titleView, artistView;

    private MediaPlayer mp;

    private DialogState state;
    private Song song;

    private Handler handler;

    protected SongInfoDialog(Context context) {
        super(context);

        setCanceledOnTouchOutside(true);

        state = DialogState.IDLE;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.dialog_layout, null);
        seekBar = (SeekBar) v.findViewById(R.id.dialog_seekBar);
        bitRateView = (TextView) v.findViewById(R.id.dialog_bitrateView);
        bitRateView.setText(R.string.loading_bitrate);
        seekBar.setOnSeekBarChangeListener(this);

        titleView = (TextView) v.findViewById(R.id.dialog_titleView);
        artistView = (TextView) v.findViewById(R.id.dialog_artistView);

        playButton = (Button) v.findViewById(R.id.playButton);
        downloadButton = (Button) v.findViewById(R.id.downloadButton);
        playButton.setOnClickListener(this);
        downloadButton.setOnClickListener(this);

        seekBar.setOnSeekBarChangeListener(this);
        handler = new Handler();
        moveSeekbarRunnable.run();

        getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;

        setView(v);
    }

    public void reset(Song song){
        if(isShowing()) dismiss();
        this.song = song;
        bitRateView.setText(R.string.loading_bitrate);
    }


    @Override
    public void show() {
        super.show();
        titleView.setText(song.title);
        artistView.setText(song.artist);
        new LoadBitrateTask().execute(song);
        checkBitrate.run();
    }

    public Song getSong(){
        return song;
    }


    private Runnable checkBitrate = new Runnable() {
        @Override
        public void run() {
            if(song.bitrate == Song.BITRATE_UNKNOWN){
                handler.postDelayed(this, 200);
            }
            else bitRateView.setText(String.valueOf(song.bitrate) + " kbps");
        }
    };

    private Runnable moveSeekbarRunnable = new Runnable() {
        @Override
        public void run() {
            if(state == DialogState.PLAYING){
                seekBar.setProgress(mp.getCurrentPosition());
            }
            handler.postDelayed(this, 2000);
        }
    };

    public void onActivityDestroyed(){
        if(mp != null) mp.stop();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(fromUser && mp.isPlaying()){
            mp.seekTo(progress);
            seekBar.setProgress(progress);
        }
    }

    private void setState(DialogState state){
        this.state = state;

        int text = 0;
        boolean enabled = false;


        switch (state){
            case PLAYING:
                text = R.string.song_pause;
                enabled = true;
                break;
            case PREPARING:
                text = R.string.loading;
                enabled = false;
                break;
            case IDLE:
            case PAUSED:
                text = R.string.song_play;
                enabled = true;
                break;
        }

        playButton.setText(text);
        playButton.setEnabled(enabled);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.downloadButton){

            //DOWNLOAD

            Intent i = new Intent(getContext(), DownloadService.class);
            i.putExtra(DownloadService.titleExtra, song.title);
            i.putExtra(DownloadService.urlExtra, song.url);
            i.putExtra(DownloadService.artistExtra, song.artist);
            getContext().startService(i);
            dismiss();
        }
        else {
            if (id == R.id.playButton) {

                //PLAY

                switch (state) {
                    case IDLE:
                        try {
                            mp = new MediaPlayer();
                            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            mp.setDataSource(song.url);
                            mp.setOnCompletionListener(this);

                            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer mp) {
                                    mp.start();
                                    seekBar.setMax(mp.getDuration());
                                    setState(DialogState.PLAYING);
                                }
                            });
                            mp.prepareAsync();
                            setState(DialogState.PREPARING);

                        } catch (Exception e) {

                        }
                        break;
                    case PAUSED:
                        mp.start();
                        setState(DialogState.PLAYING);
                        break;
                    case PLAYING:
                        mp.pause();
                        setState(DialogState.PAUSED);
                        break;
                }
            }
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mp.seekTo(0);
        seekBar.setProgress(0);
        if(mp.isPlaying()) mp.stop();
        setState(DialogState.PAUSED);
    }

    public enum DialogState{
        IDLE,
        PAUSED,
        PLAYING,
        PREPARING
    }
}
